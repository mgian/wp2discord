<?php
/*
 * Plugin Name: Wp2Discord
 * Plugin URI: https://gitlab.com/mgian/wp2discord
 * Description: Post selected posts (by category) to Discord
 * Version: 0.1
 * Author: Gianluca Montecchi
 * Author URI: https://devel.grys.it
*/

function send_post($post_id, $post) {

    if(get_option('discord_webhook_url') == null)
        return;

    $webhookURL = get_option('discord_webhook_url');
    $cat_slug = get_option('wp2discord_cat');
    $id = $post->ID;
    $pcats = wp_get_post_categories($id);
    $send_post = false;
    foreach ($pcats as $c) {
       $cat_name = get_cat_name($category_id = $c);
       if ($cat_name == $cat_slug) {
             $send_post = true;   
       }
    }
    
    if ($send_post == false) {
       return;
    }

 
    $author = $post->post_author;
    $authorName = get_the_author_meta('display_name', $author);
    $postTitle = $post->post_title;
    $permalink = get_permalink($id);
    $message = "@everyone " . $authorName . " just posted \"" . $postTitle . "\" for your reading pleasure: " . $permalink;
    $message = "@everyone New Bulbcalculator release: \n" . $permalink;
    $postData = array('content' => $message);

    $curl = curl_init($webhookURL);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($postData));
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);

    $response = curl_exec($curl);
    $errors = curl_error($curl);

    log_message($errors);
}

function log_message($log) {
      if (true === WP_DEBUG) {
            if (is_array($log) || is_object($log)) {
                error_log(print_r($log, true));
            } else {
                error_log($log);
            }
        }
}

add_action('publish_post', 'send_post', 10, 2);

function wp2discord_section_callback() {
  echo "<p>A valid Discord Webhook URL to the announcements channel is required.";
}

function wp2discord_input_callback() {

  echo '<input name="discord_webhook_url" id="discord_webhook_url" type="text" value="' . get_option('discord_webhook_url') . '">';

}

function wp2discord_input_cat_callback() {

  echo '<input name="wp2discord_cat" id="wp2discord_cat" type="text" value="' . get_option('wp2discord_cat') . '">';

}

function wp2discord_settings_init() {
 add_settings_section(
   'wp2discord_settings',
   'Wp2Discord',
   'wp2discord_section_callback',
   'general'
 );

 add_settings_field(
   'discord_webhook_url',
   'Discord Webhook URL',
   'wp2discord_input_callback',
   'general',
   'wp2discord_settings'
 );
 
 add_settings_field(
   'wp2discord_cat', 
   'Category to post',
   'wp2discord_input_cat_callback',
   'general',
   'wp2discord_settings'
 );


 register_setting( 'general', 'discord_webhook_url' );
 register_setting( 'general', 'wp2discord_cat' );
}

add_action( 'admin_init', 'wp2discord_settings_init' );
